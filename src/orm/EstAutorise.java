package orm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class EstAutorise {
	public static void main(String[] args) {
		// Connexion � la base droitAcces sur la machine localhost
		// en utilisant le login "root" et le password ""
		try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/droitAcces","root","")){
			DroitAccesDao dao = new DroitAccesDao(connection);
				System.out.println(dao.isAutorise("connexion", "doe"));
				System.out.println(dao.isAutorise("connexion", "spoonless"));
				System.out.println(dao.isAutorise("ecriture", "vincent"));
				System.out.println(dao.isAutorise("connexion", "Maxime"));
				System.out.println(dao.isAutorise("mettre une bonne note", "David"));
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
}