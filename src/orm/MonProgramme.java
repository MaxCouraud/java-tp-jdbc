package orm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class MonProgramme {
	public static void main(String[] args) {
		// Connexion � la base droitAcces sur la machine localhost
		// en utilisant le login "root" et le password ""
		try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/droitAcces","root","")){
			DroitAccesDao inst = new DroitAccesDao(connection);
			
			inst.desactiverAnciensUtilisateur();
			
			for (Utilisateur user : inst.getUtilisateurs())
			{
				String newLine = System.getProperty("line.separator");
				System.out.println("ID : "+ user.getId() +newLine+ " Login : " + user.getLogin() +newLine+ " Date inscription : " + user.getInscription() +newLine+ " Comtpe actif : " + user.isActif()+newLine);
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
}
