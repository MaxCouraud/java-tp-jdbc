package orm;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DroitAccesDao {
	public static final String DESACTIVER_ANCIENS_UTILISATEUR = "UPDATE utilisateur SET actif = false WHERE YEAR(NOW())-YEAR(dateInscription)>10";
	public static final String LISTE_UTILISATEUR = "SELECT * FROM Utilisateur";
	public static final String LIST_UTILISATEUR_HABILITE ="SELECT * FROM utilisateur u INNER JOIN Utilisateur_droit ud ON id_utilisateur = u.id INNER JOIN Droit d ON d.id=ud.id_droit WHERE d.libelle = ? AND u.login = ?";
	public static final String AJOUTER_UTILISATEUR= "INSERT INTO utilisateur (login, dateInscription, actif) values (?, ?, ?)";
	public static final String AJOUTER_DROIT_UTILISATEUR= "INSERT INTO utilisateur (login, dateInscription, actif) values (?, ?, ?)";
	

	private Connection connection;

	public DroitAccesDao(Connection connection) {
		this.connection = connection;
	}

	public void desactiverAnciensUtilisateur() throws SQLException {
		
		try (Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(DESACTIVER_ANCIENS_UTILISATEUR);
		}
	}

	public List<Utilisateur> getUtilisateurs() throws SQLException {

		ArrayList<Utilisateur> listeUtilisateurs = new ArrayList<Utilisateur>();

		try (Statement stmt = connection.createStatement();
				java.sql.ResultSet resultSet = stmt.executeQuery(LISTE_UTILISATEUR);) {

			while (resultSet.next()) {
				Utilisateur utilisateur = new Utilisateur();
				utilisateur.setId(resultSet.getInt("id"));
				utilisateur.setLogin(resultSet.getString("Login"));
				utilisateur.setInscription(resultSet.getDate("dateInscription"));
				utilisateur.setActif(resultSet.getBoolean("actif"));
				listeUtilisateurs.add(utilisateur);
			}
			return listeUtilisateurs;
		}
	}
	
	public boolean isAutorise(String login, String droit) throws SQLException {
		try (java.sql.PreparedStatement pstmt = connection.prepareStatement(LIST_UTILISATEUR_HABILITE)) {
			pstmt.setString(1, login);
		    pstmt.setString(2, droit);
		    
			try (java.sql.ResultSet resultSet = pstmt.executeQuery()) {
			      return resultSet.next();
			}
		}
	}
	//Je n'ai pas r�ussi cette partie
	/*public void addUtilisateur(Utilisateur utilisateur, String... droit) throws SQLException {
		connection.setAutoCommit(false);
		boolean transactionOk = false;

		try {
			try (java.sql.PreparedStatement pstmt = connection.prepareStatement(AJOUTER_UTILISATEUR))  {
	
			  pstmt.setString(1, login);
			  pstmt.setDate(2, new java.sql.Date(System.currentTimeMillis()));
			  pstmt.setBoolean(3, actif);
	
			  pstmt.executeUpdate();
			  
			  try (java.sql.ResultSet resultSet = pstmt.getGeneratedKeys()) {
				    if (resultSet.next()) {
				        int key = resultSet.getInt(1);
				        // ...
				    }
				  }
			}
			
			  try (PreparedStatement pstmt = connection.prepareStatement(AJOUTER_DROIT_UTILISATEUR)) {
			    pstmt.setLong(1, getIdUtilisateur); 
			    pstmt.setString(2, setIdDroit);

			    pstmt.executeUpdate();
			  }

			  transactionOk = true;
		}finally {

			  if (transactionOk) {
			    connection.commit();
			  }
			  else {
			    connection.rollback();
			  }
			}
		
	}*/

}
